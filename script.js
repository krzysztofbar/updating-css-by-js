var inputs = document.querySelectorAll('.controls input');

function update() {
    var myData = this.dataset.sizing || '';
    document.documentElement.style.setProperty(`--${this.name}`, this.value + myData);
}

inputs.forEach(input => input.addEventListener('change', update));
inputs.forEach(input => input.addEventListener('mousemove', update));